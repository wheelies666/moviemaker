xxxxxxxxxxxxMovieMaker.rb needs a few things (common on most linux distros) to fully work.

System tools:
a working ffmpeg(compiled with libass) in your PATH
a working Imagemagick in your PATH

Ruby Gems:
imdb
optparse
rqrcode
rqrcode_png
fileutils


KDE .desktop entry to add right clicking on a directory and having Actions -> Make imdb.com Movie
text file named MovieMaker.desktop with the following contents and placed wherever your distro needs .desktop files.
[Desktop Entry]
ServiceTypes=inode/directory
Actions=MakeMovie;
Type=Service
X-KDE-ServiceTypes=KonqPopupMenu/Plugin

[Desktop Action MakeMovie]
Name=Make imdb.com Movie
Icon=video-x-generic
Exec=konsole --workdir %u -e  MovieMaker.rb -q 128 -f -d %u