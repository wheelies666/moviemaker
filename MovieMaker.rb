#!/usr/bin/env ruby

#Makes simple 1 minute movies containing movie cover and some basic info pulled from imdb.  This allows minidlna (xbox/ps3) users to view imdb info w/o leaving the video selection screen.

#This script requires the imdb gem (gem install imdb) or head over to 				https://rubygems.org/gems/imdb.
#This script requires the rqrcode gem (gem install rqrcode) or head over to			https://rubygems.org/gems/rqrcode
#This script requires the rqrcode_png  gem (gem install rqrcode_png) or head over to	https://rubygems.org/gems/rqrcode_png


#Assumes a working ffmpeg install with --enable-libass in your PATH. 					https://www.ffmpeg.org/
#Assumes a working Imagemagick(convert/composite) in your PATH					http://www.imagemagick.org/

#TODO:
# Rewrite the ffmpeg stuff using ffmpeg gem maybe?
# check that passed --directory actually exists and handle gracefully if not.
# check that passed  --allow-only is formatted correct, handle gracefully if not.
# do not allow use of -q if 'rqrcode' gem and 'rqrcode_png' gem are not installed. those should not be needed if qrcode is not wanted at all. currently script breaks if not installed, it should not.

#Valid methods to use with my_movie.<one of the following>
#cast_characters, #cast_member_ids, #cast_members, #cast_members_characters, #company, #countries, #director, #filming_locations
#genres, #languages, #length, #mpaa_rating, #plot, #plot_summary, #plot_synopsis, #poster, #rating, #release_date
#tagline, #trailer_url, #votes, #writers, #year

######KDE .desktop entry to add right clicking on a directory and having Actions -> Make imdb.com Movie 
######This runs MovieMaker.rb script with the -q 128(embed a 128 pixel qrcode in top right of movie), -f (first search result) and -d(directory) passed to it from KDE and automates it all.
######Goes in /home/YOUR_USERNAME/.kde/share/kde4/services/ServiceMenus(Slackware)  or where ever your distro places ServiceMenu .desktop files.
######or just run this script recursive with 'find' and pass -q 128 -f -d DIRECTORY to it. winders users can... umm... nm, no idea.
# [Desktop Entry]
# ServiceTypes=inode/directory
# Actions=MakeMovie;
# Type=Service
# X-KDE-ServiceTypes=KonqPopupMenu/Plugin
# 
# [Desktop Action MakeMovie]
# Name=Make imdb.com Movie
# Icon=video-x-generic
# Exec=konsole --workdir %u -e  MovieMaker.rb -q 128 -f -d %u

# or maybe just use:
# find /home/<username>/path/to/movies/dir -type d -mindepth 1 -exec MovieMaker.rb -f -d {} \;

#############################################################################################################
require 'imdb'
require 'optparse'
require 'rqrcode'
require 'rqrcode_png'
require 'fileutils'
require 'tmpdir'

options = {:smallfont => 20, :largefont => 26, :wordwrap => 90, :coversize =>340, :directory => false, :first => false, :keepfiles => false, :qrcode => false, :allow => false, :verbose => false, :tmpdir => false}

script_name = File.basename($0)

optparse = OptionParser.new do |opts|

  
opts.banner = "\nDownload cover art, query imdb for info, build a 1 min movie from it all.\n\n\
  Usage: #{script_name} [options] \[movie_to_find\]\n\
  If no --directory is given, created move is moved to the current working directory\n\n\
    Examples:\n\
      \t#{script_name}\t\t\t\t\t\t\tprompts user for movie and on multiple results, user selects out of list. shows only cinema movies\n\
      \t#{script_name} \"top gun\"\t\t\t\t\t\tsearch for \"top gun\" and on multiple results, user selects out of list. shows only cinema movies\n\
      \t#{script_name} -f -d \"path/to/directory_named_as_movie\"\t\tuse directory name as search then accept the first result.  Movie copied to directory\n\
      \t#{script_name} -a TV_MOVIE,TV_SERIES,TV_MINI_SERIES \"shrek 2\" \tsearch for \"shrek 2\" and allow only tv movies, tv mini series and tv series(NO spaces!)in the result \n\
      \t#{script_name} -a ALL \"die hard\" \t\t\t\tsearch for \"die hard\" and allow anything in the result \n\
  "
    
    opts.on("-s", "--small-font FONTSIZE", Integer, "size of small font in subs (default is 20)") do |smallfont|
      options[:smallfont] = smallfont || 20
    end
    
    opts.on("-l", "--large-font FONTSIZE", Integer, "size of large font in subs (default is 26)") do |largefont|
      options[:largefont] = largefont || 26
    end
    
    opts.on("-w", "--word-wrap WRAPSIZE", Integer, "word wrap length for plot (default is 90)") do |wordwrap|
      options[:wordwrap] = wordwrap || 90
    end
    
    opts.on("-c", "--cover-size [SIZE]", Integer,  "max height in pixels of cover (default is 340)") do |coversize|
      options[:coversize] = coversize || 340
    end

    opts.on("-d", "--directory DIRECTORY", "get file name from DIRECTORY \(use -f here as well for autoselection\)") do |directory|
    options[:directory] = directory
    end
    
    opts.on("-f", "--first-option", "use first movie in search list") do |first|
      options[:first] = first
    end
    
    opts.on("-k", "--keep-files", "keep temp files we create (default is delete)") do |keepfiles|
    options[:keepfiles] = keepfiles
    end
    
    opts.on("-q", "--qrcode [SIZE]", Integer, "create qrcode SIZE in pixels and put in top right of movie") do |qrcode|
    options[:qrcode] = qrcode || 128
    end
    
    opts.on("-a", "--allow-only a,b,c,d,e,f,g", Array, "ALL or comma seperated(no spaces!)combination of these:\n\t\t\t\t\tTV_SERIES TV_MINI_SERIES TV_MOVIE VIDEO_GAME SHORT VIDEO IN_DEVELOPMENT") do |allow|
    options[:allow] = allow
    end

    opts.on("-v", "--verbose", "run verbosely") do |v|
    options[:verbose] = v
    end

    opts.on('-h', '--help', 'displays this help') do
    puts opts
    exit
    end
end

optparse.parse!

#############################################################################################################
def user_query()
  
  puts "(\'exit\' will exit this script)"
  STDOUT.flush
  searchstring = STDIN.gets.chomp
  
  if searchstring == "exit"
    exit
  end
  
  return searchstring
end

#############################################################################################################
def search(searchstring, options)

system("clear") if !options[:verbose]
searchResults = Imdb::Search.new("#{searchstring}")
puts "Searching..."
filter(searchResults, options)
end

#############################################################################################################
def filter(searchResults, options)

display_arr = Array.new
  
searchResults.movies.each do |x|			
	if x.title.match(/^-.*$/)																								#always filter lines starting with a - char.
	  next
	end
	
	if options[:allow]																									#did user set -a ?
	  if options[:allow].include? 'ALL'; display_arr.push(x)																	#if -a was set and equals "all" ? If so show every search result.
 	  elsif x.title.match(/.*\(TV Series\).*/) && (options[:allow].include? 'TV_SERIES'); display_arr.push(x)
  	  elsif x.title.match(/.*\(Video\).*/) && (options[:allow].include? 'VIDEO'); display_arr.push(x) 
  	  elsif x.title.match(/.*\(Short\).*/) && (options[:allow].include? 'SHORT'); display_arr.push(x) 
  	  elsif x.title.match(/.*\(Video Game\).*/) && (options[:allow].include? 'VIDEO_GAME'); display_arr.push(x) 
  	  elsif x.title.match(/.*\(in development\).*/) && (options[:allow].include? 'IN_DEVELOPMENT'); display_arr.push(x)
	  elsif x.title.match(/.*\(TV Movie\).*/) && (options[:allow].include? 'TV_MOVIE'); display_arr.push(x)
	  elsif x.title.match(/.*\(TV Mini-Series\).*/) && (options[:allow].include? 'TV_MINI_SERIES'); display_arr.push(x)
	  end
	elsif x.title.match(/.*\(TV Mini-Series\).*/) || x.title.match(/.*\(TV Movie\).*/) || x.title.match(/.*\(TV Series\).*/) || x.title.match(/.*\(Video\).*/) || x.title.match(/.*\(Short\).*/) || x.title.match(/.*\(Video Game\).*/) || x.title.match(/.*\(in development\).*/)
	    next
	else
	  display_arr.push(x)
	end
end
display(display_arr, options)
end

#############################################################################################################
def display(display_arr, options)

system("clear") if !options[:verbose]

#display an orderly list of whats left after the above FILTERing takes place
if display_arr.size == 0													#Did we get 0 results ?
    puts "No results, Please try again.\n\nMovie to search for?"
    user_input = user_query()											#ask user for new search term.
    search(user_input, options)											#call Search() again with new term.
else
  #system("clear") if !options[:verbose]
  display_arr.each_with_index{|v,i|  puts i.to_s + "\t\t" + v.title}				#show the (filtered) search results to the user.
  puts "\nthere are " + display_arr.size.to_s + " movies that match"			#Ok, got some results, ask user which one to use.
  puts "please choose the one you want "
    if options[:first]														#does user want the first result taken automatically
      puts "-f was passed, using first search result"
      user_input = 0
      system("clear") if !options[:verbose]
    else
      	  begin
	    user_input = user_query()
	    user_input = Integer(user_input)
	    if user_input >= display_arr.length
	      raise "Input was an integer, but it was not a valid selection"
	    end
	  rescue
	    #system("clear") if !options[:verbose]
	    puts "Not a valid selection!"
	    sleep 2
	    retry
	  end
    end
  my_movie = display_arr[user_input]											#get the array value of ONLY the movie the user selected.
  get_movie_data(my_movie.id, options)										#return ONLY the 'id' inside the user selected movie array.
end
end

#############################################################################################################
def get_movie_data(movie_id, options)

my_movie = Imdb::Movie.new(movie_id)
makeSUBS(my_movie, movie_id, options)
end

#############################################################################################################
def makeSUBS(my_movie, movie_id, options)

system("clear") if !options[:verbose]

rating = my_movie.rating.to_s												#Should check these exist, but if they are on imdb we can assume it has votes(and thus a rating)
votes = my_movie.votes.to_s												#Should check these exist, but if they are on imdb we can assume it has votes(and thus a rating)

if my_movie.genres.nil?
  genres_string = "No genre listed"
else
  genres = my_movie.genres
  genres_string = ""
  genres.each do |x|
  genres_string << "#{x} "
end
end

if my_movie.mpaa_rating.nil?
  mpaa_rating = "Not Rated"
else
  mpaa_rating = my_movie.mpaa_rating
end

if my_movie.plot_summary.nil?
  puts "No plot listed"
  word_wrapped_string = "No Plot Summary given at imdb.com\n\n"
else
  stripped_string = my_movie.plot_summary
  stripped_string.gsub!(/<\/?[^>]*>/, "")
  word_wrapped_description = stripped_string.scan(/\S.{0,#{options[:wordwrap]}}\S(?=\s|$)|\S+/)
  word_wrapped_string = ""
  word_wrapped_description.each do |x|
  word_wrapped_string << ("#{x}" + "\\N")
  end
end

puts "Creating subtitle file"

subfile = <<-SUBS
[Script Info]
ScriptType: v4.00+
Collisions: Normal
PlayResX: 1280
PlayResY: 720
PlayDepth: 0

[V4+ Styles]
Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, AlphaLevel, Encoding
Style: Default,Arial,16,&Hffffff,&Hffffff,&H0,&H0,0,0,0,1,1,0,2,10,10,10,0,0

[Events]
Format: Layer, Start, End, Style, Text
Dialogue: 0,0:00:00.00,0:01:00.00,Default,{\\fs#{options[:smallfont]}}\\Nimdb.com rating of #{rating} from #{votes} users.\\N#{genres_string}\\NRating: #{mpaa_rating}{\\fs}{\\fs#{options[:largefont]}}\\N#{word_wrapped_string}\\N{\\fs}
SUBS

file = File.open("#{options[:tmpdir]}/Subs.ass", "w")
file.puts subfile
file.close
  
downloadPoster(my_movie, movie_id, options)
end

#############################################################################################################
def downloadPoster(my_movie, movie_id, options)

if my_movie.poster.nil?
  options[:verbose] ? ( verb =  "-verbose" ) : ( verb = nil )
  puts "Missing Cover art from IMDB.\nCreating blank cover...\n\n"
system("convert #{verb} -size 1280x720 canvas:black -quality 20 #{options[:tmpdir]}/Full_Cover.jpg")
else
  puts "Downloading Cover"
  uri = URI(my_movie.poster)
  cover = Net::HTTP.get(uri)
  File.open("#{options[:tmpdir]}/Full_Cover.jpg", "w"){ |file| file.write("#{cover}") }
end
makeImages(my_movie, movie_id, options)
end

#############################################################################################################
def makeImages(my_movie, movie_id, options)

if options[:qrcode]
  url = "http://www.imdb.com/title/tt#{movie_id}/"
  qr = RQRCode::QRCode.new("#{url}", :size => 4, :level => :l )
  qr_data = qr.to_img
  qr_data.resize(options[:qrcode], options[:qrcode]).save("#{options[:tmpdir]}/qr_data.png")
end
  
options[:verbose] ? ( verb =  "-verbose" ) : ( verb = nil )

puts "Creating Images for background"
  system("convert #{verb} -size 1280x720 canvas:black -quality 20 #{options[:tmpdir]}/Base_Image.jpg")														#Create background image
  system("convert #{verb}  #{options[:tmpdir]}/Full_Cover.jpg -quality 50 -resize 1280x#{options[:coversize]} #{options[:tmpdir]}/Cover.png")					#Resize cover to 360px tall
  system("composite #{verb} -gravity north #{options[:tmpdir]}/Cover.png #{options[:tmpdir]}/Base_Image.jpg #{options[:tmpdir]}/Video.jpg")					#overlay cover onto backround
  system("composite #{verb} -gravity north-east #{options[:tmpdir]}/qr_data.png #{options[:tmpdir]}/Video.jpg #{options[:tmpdir]}/Video.jpg") if options[:qrcode]	#overlay our qrcode image onto Video.jpg
  
ffmpeg(options)
end

#############################################################################################################
def ffmpeg(options)

puts "Creating Movie"

options[:verbose] ? (verb = "") : (verb = "-loglevel panic")
  system("ffmpeg #{verb} -y -loop 1 -i #{options[:tmpdir]}/Video.jpg -c:v libx264 -preset slow -vf subtitles=#{options[:tmpdir]}/Subs.ass -t 60 #{options[:tmpdir]}/About_this_Movie.avi")
  cleanup(options)

end

#############################################################################################################
def cleanup(options)
puts "in cleanup"
!options[:directory] ? options[:directory] = FileUtils.pwd : nil																								# no -d passed, so we'll set it to user current directory

if options[:keepfiles]
  FileUtils.cp(Dir.glob("#{options[:tmpdir]}/*"), "#{options[:directory]}", :verbose => options[:verbose])
else
  FileUtils.cp("#{options[:tmpdir]}/About_this_Movie.avi", "#{options[:directory]}", :verbose => options[:verbose])
end	
  
  #copies finished, clear the tmpdir
puts "cleaning up"
files_to_remove = Array["#{options[:tmpdir]}/About_this_Movie.avi", "#{options[:tmpdir]}/Base_Image.jpg", "#{options[:tmpdir]}/Cover.png", "#{options[:tmpdir]}/Full_Cover.jpg", "#{options[:tmpdir]}/Subs.ass", "#{options[:tmpdir]}/Video.jpg"]
files_to_remove.each { |f| FileUtils.rm(f, :verbose => options[:verbose]) }
FileUtils.rm("#{options[:tmpdir]}/qr_data.png", :verbose =>  options[:verbose]) if File.exists?("#{options[:tmpdir]}/qr_data.png")
puts "Done!"
end 

######################################################Start Here#################################################
system("clear"); (0..3).each { (31..37).each { |i| print "\e[1;" + "#{i}" + "mScript by Wheelies\r"; sleep 0.1 }}; print "\r\n"; print "\e[0;37mScript by Wheelies"; system("clear"); sleep 0.5

#user_query -> search -> filter -> display -> user_query -> get_movie_data -> makeSUBS -> downloadPoster -> ffmepg
#	    |							|
#	    |-------------------<-------------------|				# no results found

options[:tmpdir] = Dir.mktmpdir()
# puts options[:tmpdir]
# sleep 5

if options[:directory]
  basedir = File.basename(options[:directory])
    if basedir =~ /\(.*\d{4}\)/						#if dir name has (<anything><4 digits>) in it
      temp = basedir.match(/^.*(?=\(\d{4}\))/)					#works for names like 'anything (<anything><4 digits>) anything'
      searchstring = temp[0]							#ex. 'Body of Evidence (1993) [1080p]' or 'Underworld Awakening (2012) [1080p]'
      searchstring.gsub!(/\./, " ")						#replace . with <space>
    elsif basedir =~ /\.\d{4}\./						#if dir name has .<4 back to back digits>. in it
      temp = basedir.match(/^.*(?=\d{4})/)				#everything up to, but not including <4 digits>
      searchstring = temp[0]							#ex. 'A.Million.Ways.To.Die.In.The.West.2014.WEBRip.HC.XviD.MP3-RARBG'
      searchstring.gsub!(/\./, " ")						#replace . with <space>
    end
elsif ARGV[0]
  searchstring = ARGV[0]
else
  print "Movie to search for? "
  searchstring = user_query()
end

search(searchstring, options)
